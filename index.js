const bodyParser = require("body-parser");
const express = require("express");
const app = express();
const server = require("http").Server(app);
const io = require("socket.io")(server);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static("public"));

const users = ['Global'];
const room1 = io.of("/room1");
const roomno =2;

room1.on("connection", (socket) => {
  console.log(socket.id + " connected");
  socket.emit("hi", "Hello everyone!");
  //Increase roomno 2 clients are present in a room.
  if (
    io.nsps["/"].adapter.rooms["room-" + roomno] &&
    io.nsps["/"].adapter.rooms["room-" + roomno].length > 1
  )
    roomno++;
  socket.join("room-" + roomno);

  //Send this event to everyone in the room.
  io.sockets
    .in("room-" + roomno)
    .emit("connectToRoom", "You are in room no. " + roomno);
});
//socket.adapter.rooms => hienr thij ds cac room dc tao ra


io.on("connection", (socket) => {
  console.log(socket.id + " connected");
  
  socket.on("chat", data=>{
    console.log(data);
  })
  socket.on("check-name", (name) => {
    if (users.filter((c) => c.name === name).length === 0) {
      socket.emit("check-success", name);
      socket.userName = name;
      users.push({name, id: socket.id});
      io.sockets.emit("send-users", users);
    } else socket.emit("check-fail");
  });
  socket.on("client-message", (data={}) => {
    if (data && data.name === "Global")
      socket.broadcast.emit("server-message", {...data,name:socket.userName});
    else {
      console.log(io.sockets);
      // let user = io.sockets.find(c=> c.userName === data.name)
      // io.to(user.id).emit("pm-message", {name: socket.userName, message: data.message})
    }
  });
  socket.on("logout", () => {
    users = users.filter(c=> c.name!=socket.userName)
    console.log(socket.userName + " logout");
    socket.userName = null;
    io.sockets.emit("send-users", users);
  });
  socket.on("disconnect", () => {
    if (socket.userName != null) {
      users = users.filter(c=> c.name!=socket.userName)
      console.log(socket.userName + " disconnected");
      socket.userName = null;
      io.sockets.emit("send-users", users);
    }
  });
});

app.get("/", (req, res) => {
  res.sendFile(__dirname + "/views/chat.html");
});

server.listen(5000, () => {
  console.log("server is running...");
});
