$(document).ready(function () {
  const socket = io("http://localhost:5000/");

  const chat = document.getElementById("add-node");
  const nodesender = document.getElementById("sender");
  const nodeResive = document.getElementById("resive");
  const inputMessage = document.getElementById("send");
  const scrollEnd = () => {
    document
      .getElementById("message-end")
      .scrollIntoView({ behavior: "smooth" });
  };

  //AUTO RUN AT START
  scrollEnd();
  
  document.getElementById("form").addEventListener("click", function (event) {
    event.preventDefault();
  });
  document.getElementById("chat").style.display = "none";

  //RUN WHEN BE CALL
  window.sendMessage = () => {
    event.preventDefault();
    let message = inputMessage.value;
    let name = document.querySelector("#list-tab > a.active").textContent;
    socket.emit("client-message", {name,message});
    chat.appendChild(createMessageNode(message));

    inputMessage.value = "";
    scrollEnd();
  };
  window.handleRegister = () => {
    event.preventDefault();
    let name = document.getElementById("name").value;
    socket.emit("check-name", name);
  };
  const createMessageNode = (data, type = "sender") => {
    let node = type === "sender" ? nodesender : nodeResive;
    node = node.cloneNode(true);
    node.classList.add("edit-item");
    node.textContent = data;
    return node;
  };

  window.logout = () => {
    document.getElementById("chat").style.display = "none";
    document.getElementById("dangki").style.display = "block";
    socket.emit("logout");
  };
  window.chat = (name) => {
    socket.emit("chat", name);
    let node = document.getElementsByClassName("list-group-item");
    for (let i = 0; i < node.length; i++) {
      node[i].classList.remove("active");
      if (node[i].innerHTML === name) node[i].classList.add("active");
    }
    document.querySelector("#add-node").textContent = "";
  };

  //AUTO RUN
  socket.on("server-message", (data) => {
    $("#add-node").append(`<span>${data.name}</span>`)
    chat.appendChild(createMessageNode(data.message, "resive"));
    scrollEnd();
  });

  socket.on("check-success", (data) => {
    document.getElementById("chat").style.display = "block";
    document.getElementById("dangki").style.display = "none";
    document.getElementById("user-name").textContent = "Welcome " + data;
  });
  socket.on("connectToRoom", (data) => {
    alert(data);
  });
  socket.on("check-fail", () => {
    alert("This name was be use.");
  });
  socket.on("send-users", (data = []) => {
    let users = document.querySelectorAll(".list-group-item.list-group-item-action");
    let tab = document.querySelectorAll("#nav-tabContent");

    for (let i = users.length - 1; i >= 0; i--) {
      if (data.includes(users[i].textContent))
        data.splice(data.indexOf(users[i].textContent), 1);
      else {
        users[i].remove();
        tab[i].remove();
      }
    }

    if (data.length === 0) return;

    data.forEach((c) => {
      $("#list-tab").append(
        `<a class="list-group-item list-group-item-action" id="list-settings-list" data-toggle="list" href="#list-settings" role="tab" aria-controls="settings" onclick="window.chat('${c}')">${c}</a>`
      );
    });
  });
  socket.on("pm-message", data=>{
    console.log("pm message",data);
  })
});
